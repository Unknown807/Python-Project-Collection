disnum = input()
_sum = 0
for num, item in enumerate(disnum, 1):
    calc = int(item)**num
    _sum+=calc

if _sum == int(disnum):
    print("True")
else:
    print("False")

#As an anonymous function

anonfunc = lambda num: "True" if int(num)==sum(int(item)**power for power, item in enumerate(num, 1)) else "False"

print(anonfunc(disnum))
