def crys(string, sett=False):
 if sett:
  start, end, step = len(string)-1, -1, -1
 else:
  start, end, step = 0, len(string), 1
 for x in range(start, end, step):
     count=0
     temp=[]
     if x+1 == len(string)+1:
         break
     else:
         print(" "*(len(string)-x), end="")
         for y in range(x+1):
            print(string[count], end="")
            temp.append(string[count])
            count+=1
         print(" "+"".join(temp[::-1])+"\n")
#Enter a word and watch the magic
user_input = input()
print("---------CRYSTAL----------")
crys(user_input)
crys(user_input, True)
print("--------HOURGLASS-----------")
crys(user_input, True)
crys(user_input)
print("--------XMAS-TREE-----------")
crys(user_input)
crys(user_input)
