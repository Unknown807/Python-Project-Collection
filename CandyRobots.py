
def produceCandy(pay, noBots, EffHours, Candies, botPrice):
    time = pay//botPrice

    NonEffTime = time - (EffHours * noBots)
    if NonEffTime < 0:
        candyMade = time * Candies
    else:
        candyMade = ((time-NonEffTime) * Candies) + NonEffTime
        
    return candyMade

numOfBots = int(input("Enter the number of robots you want: "))
moneyToUse = int(input("Enter how much money you got to pay for this: "))
timeForCandy = int(input("Enter the amount of time the\nthe robots can work efficiently: "))
perHourCandies = int(input("Enter the amount of candies robots\ncan produce during efficient work: "))
priceForBot = int(input("Enter the price you are willing to hire the robots at: "))

print('''Given the money you had and robots
you had you were able to produce
''',produceCandy(moneyToUse, numOfBots,
                 timeForCandy, perHourCandies,
                 priceForBot), "Candies")


##The Challenge:
##    robots in a factory make C candies efficiently every hour for
##    H consecutive hours, until they start to break down and can only
##    make 1 candy every hour afterwards. Print out the number of candies a
##    specified number of robots can make.
##
##    INPUT:
##        N, C, = Number of robots to be hired, the cost of each robot
##
##    OUTPUT:
##        Given the amount of money paid and robots hired, an output of
##        how many candies were able to be efficiently produced should be
##        given.
