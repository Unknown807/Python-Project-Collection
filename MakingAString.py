#This code finds the cheapest way to form a string S of length N
#according to the cost of appending one character to the end of the new string (A dollars)
#and the cost of appending a substring to the end of the new string (B dollars)


def makestring(a, b, s):
    string=""
    cost=0
    count=0
    #loops through the whole string
    while count<len(s):
        in_count=len(s)
        #if the character is already in the string
        #see how many other chars to copy as well
        if s[count] in string:
            #for that character going from to end of the list to sai d character
            while in_count != count:
                to_add=s[count:in_count:]#keeps decreasing from the end
                if to_add in string:
                #if that substring is in string then copy
                    if b > len(to_add)*a: cost+=len(to_add)*a
                    else: cost+=b#first check is it cheaper to copy though
                    string+=to_add#then add the string
                    count+=(in_count-count)#followed by moving count to
                    #the character at the end of the substring just copied
                    #in order to prevent any string duplication.
                    break
                in_count-=1#if its not in string, keeps moving back to find
                #the substring that is
        else:
            #otherwise just append the one character and move on to the next
            string+=s[count]
            cost+=a
            count+=1
    return (string, cost)

print(makestring(4,5,"aabaacaba"))
print(makestring(8,9, "bacbacacb"))
