#getter and setter
class Employee(object):
#apparantly its more pythonic to make your class inherit from 'object', unless of course you need to make a subclass of another class.
    def __init__(self, fname, lname, pay):
        self.fname = fname
        self.lname = lname
        self.pay = pay
    
    @property
    def fullname(self):
        return "{}, {}".format(self.fname, self.lname)
        
    @fullname.setter
    def fullname(self, newname):
        x, y = newname.split(" ")
        self.fname = x
        self.lname = y

    @fullname.deleter
    def fullname(self):
        self.fname = None
        self.lname = None

emp1 = Employee("dave", "david", 75000)


print(emp1.fullname)
emp1.fullname = "John Doe"
print(emp1.fullname)
del emp1.fullname
#doing del emp1 would just delete the entire obejct
print(emp1.fullname)
