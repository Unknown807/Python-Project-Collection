words = input("Enter a bunch of words separated with commas: ").split(",")

for w in words:
    if len(w) > 11:
        print("Words must be under 10 characters or below")
        quit()
        
sd = {1: ("e", "a", "i", "o", "n", "r", "t", "l", "s", "u"),
      2: ("d", "g"),
      3: ("b", "c", "m", "p"),
      4: ("f", "h", "v", "w", "y"),
      5: "k",
      8: ("j", "x"),
      10: ("q", "z")}

result = [0, None]
for x in words:
    _sum = 0
    for y in x:
        for k, v in sd.items():
            for z in v:
                if y.lower() == z:
                    _sum+=k
    if _sum > result[0]:
        result[0]=_sum
        result[1]=x
        
print("The highest scoring word was",result[1],"with",result[0],"points")

#it definitely would of been easier to just assign each key a separate value
#even if they were the same values, but this way was much more interesting
