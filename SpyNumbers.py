#spy numbers
#example 1,2,3,4
from functools import reduce
result = [int(n) for n in input().split(",")]
result = reduce(lambda x, y: x*y, result) == sum(result)

print(result)
