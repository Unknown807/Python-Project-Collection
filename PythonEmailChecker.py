import re

email_input = input()

email_format = r"(\w)+([\.\-](\w+))*@(\w)+(\.(\w+))+\Z"

if re.match(email_format, email_input):
    print("match")
else:
    print("no match")


# a word followed by a . or - followed by a word (this can occur multiple times, must end in a word) then the @ symbol followed by a word followed by . and a word, (can occur multiple times such as .co.uk) but it must end in that format.
