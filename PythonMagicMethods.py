class Queue:
    def __init__(self, contents):
        self._hiddenlist = contents
        #works the same as list(contents)

    def push(self, value):
        self._hiddenlist.insert(0, value)

    def pop(self):
        return self._hiddenlist.pop(-1)

    def __repr__(self):
        return "Queue({})".format(self._hiddenlist)

    def __getitem__(self, index):
        return self._hiddenlist[index]

    def __mul__(self, num):
        self._hiddenlist * num

    def __setitem__(self, index, new_index):
        self._hiddenlist[index] = new_index

queue = Queue([1, 2, 3])
print(queue[0] * 3)
print(queue._hiddenlist)
queue[2] = 45
print(queue._hiddenlist)
